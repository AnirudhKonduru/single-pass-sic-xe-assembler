#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void toString(char str[], int num)
{
	int i, rem, len = 0, n;
	n = num;
	while(n!=0)
	{
		len++;
		n /= 10;
	}
	for(i=0; i<len; i++)
	{
		rem = num%10;
		num /= 10;
		str[len - (i+1)] = rem + '0';
	}
	str[len] = '\0';
}

int decVal(char v[])
{
	int num = 0, i, a;
	for(i=0; i<strlen(v); i++)
	{
		a = v[i] - '0';
		num = num*10 + a;
	
	}
	return num;
}

int hexVal(char v[])
{
	int num = 0, i, a;
	for(i=0; i<strlen(v); i++)
	{
		a = v[i] - '0';
		num = num*16 + a;
	
	}
	return num;
}


