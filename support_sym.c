#include<stdio.h>
#include<stdlib.h>
#include<string.h>


extern FILE *output;
extern FILE * objfile;
int LOCCTR = 0;
int base_reg = 0;
extern int prev_mod, base_reg;

#include"support_records.c"


struct list
{
	int ref;
	int isIndx;
	int isExt;
	struct list * n;
};
typedef struct list* undef;

struct sym
{
	char *name;
	int loc;
	int isdef;
	struct sym * next;
	undef h;
};
typedef struct sym* symtab;

symtab head;

void createSymtab()
{
	head = (symtab)malloc(sizeof(symtab));
	head->loc = 0;
	head->name = "SYMBOL TABLE";
	head->isdef = 1;
	head->next = NULL;
	printf("\nSymtab Created");

}

int getSymLoc(char s[])
{
	symtab temp;
	temp = head;
	while(temp != NULL)
	{
		if(strcmp(temp->name, s) == 0)
			return temp->loc;
		temp = temp->next;
	}
	if(temp == NULL)
	{
		return -1;
	}
}

void dispSymtab()
{
	symtab temp;
	temp = head;
	printf("\n\n\t  %s\n",temp->name);
	temp = temp->next;
	printf("---------------------------------\n");
	while(temp != NULL)
	{
		printf("|\t%s\t|\t%x\t|\n", temp->name, temp->loc);
		printf("---------------------------------\n");
		temp = temp->next;
	}
}


void addToSymtab(char s[])
{
	symtab temp, node;
	node = (symtab)malloc(sizeof(symtab));
	node->name = s;
	node->next = NULL;
	node->isdef = 1;
	node->loc = LOCCTR;
	temp = head;
	while(temp->next != NULL)
	{
		temp = temp->next;
	}
	temp->next = node;

}

void addUndefToSymtab(char s[])
{
	symtab temp, node;
	node = (symtab)malloc(sizeof(symtab));
	node->name = s;
	node->next = NULL;
	node->isdef = 0;
	node->loc = -1;
	node->h = (undef)malloc(sizeof(undef));
	node->h->ref = -1;
	node->h->n = NULL;

	temp = head;
	while(temp->next != NULL)
	{
		temp = temp->next;
	}
	temp->next = node;

}

int isFoundSym(char s[])
{
	symtab temp;
	temp = head;
	while(temp != NULL)
	{
		if(strcmp(temp->name, s) == 0)
			return 1;
		temp = temp->next;
	}
	if(temp == NULL)
	{
		return 0;
	}
}

int isDefined(char s[])
{
	symtab temp;
	temp = head;
	while(temp != NULL)
	{
		if(strcmp(temp->name, s) == 0 && temp->isdef == 1)
			return 1;
		temp = temp->next;
	}
	if(temp == NULL)
	{
		return 0;
	}
}


void defineSymNow(char s[])
{
	int r, v, o, disp, pc;
	symtab temp;
	undef t;
	temp = head;
	v = LOCCTR;
	while(temp != NULL)
	{
		if(strcmp(temp->name, s) == 0 && temp->isdef == 0)
		{
			temp->isdef = 1;
			temp->loc = v;
			t = temp->h;
			t = t->n;
			while(t!=NULL)
			{
			    fprintf(output, "\nT^");
				r = t->ref;
				fprintf(output, "%06x^", r);
				prev_mod = ftell(output);
				o = t->isIndx;
				if(t->isExt == 0)
				{
					pc = r+2;
					if(v-pc > 4095)
					{
						o = (o<<2) + 2;
						disp = (v - base_reg)&0x00000fff;
					}
					else
					{
						o = (o<<2) + 1;
						disp = (v - pc)&0x00000fff;
					}
					o = o<<1;
					fprintf(output, "02^%01x%03x", o, disp);
				}
				else
				{
					o = (o<<1) + 1;
					fprintf(output, "03^%01x%05x", o, LOCCTR);
				}
				t = t->n;
			}
		}
		temp = temp->next;
	}
}


int findDisp(int l, char s[])
{
	int sl = 0;
	sl = getSymLoc(s);
	return sl-l;
}

void dispUndefs()
{
	symtab temp;
	undef t;
	temp = head;
	while(temp != NULL)
	{
		printf("\n%s----%x", temp->name, temp->loc);
		if(temp->isdef == 0)
		{
			t = temp->h;
			while(t!=NULL)
			{
				printf("\t@%x", t->ref);
				t = t->n;
			}
		}
		temp = temp->next;
	}
}

int addUndefRef(char s[], int l, int i, int e)
{
	symtab temp;
	undef t, r;
	temp = head;
	while(temp != NULL)
	{
		if(strcmp(temp->name, s) == 0)
		{
			r = (undef)malloc(sizeof(undef));
			r->ref = l;
			r->isIndx = i;
			r->isExt = e;
			r->n = NULL;
			t = temp->h;
			while(t->n != NULL)
				t = t->n;
			t->n = r;
			return 0;
		}
		temp = temp->next;
	}
	if(temp == NULL)
	{
		return -1;
	}
}

void calcDispAddText(char s[], int op, int indx)
{
	int disp;
	op = (op<<4) + indx;
	if(isFoundSym(s) && isDefined(s))
	{
		if(disp > 4095)
		{
			disp = findDisp(base_reg, s);
			op += 4;
			addT3(LOCCTR, op, disp);
		}
		else
		{
			disp = findDisp(LOCCTR+3, s);
			op += 2;
			addT3(LOCCTR, op, disp&0x00000fff);
		}
	}
	else
	{
		addT3(LOCCTR, op, 0);
		if(!isFoundSym(s))
			addUndefToSymtab(s);
		addUndefRef(s, LOCCTR+1, indx, 0);
	}
}

void addF4Text(char s[], int op, int indx)
{
	if(isFoundSym(s) && isDefined(s))
	{
		op = (op<<4) + (indx+1);
		addT4(LOCCTR, op, getSymLoc(s));
	}
	else
	{
		op = (op<<4);
		addT4(LOCCTR, op, 0);
		if(!isFoundSym(s))
			addUndefToSymtab(s);
		addUndefRef(s, LOCCTR+1, indx, 1);
	}
}

int NotDefinedError()
{
      symtab temp;
      temp = head;
      while(temp!=NULL)
      {
            if(temp->isdef == 0)
            return 1;
            temp = temp->next;
      }
      return 0;
}
