%{
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include"support.c"
#include"support_sym.c"
#include"support_opcode.c"
#include"support_reg.c"
char* first_exec_loc = "";
int indx = 0;
extern FILE *yyin;
FILE *output;
FILE * objfile;
extern int cur_bytes_written, isfirst_define_text;
int start_addr;
extern void modify_text_len();

#define LOCFMT "\n%06x"

%}

%union
{
	int number;
	char* string;
}

%token start
%token <string> label
%token <string> num
%token <string> opcode
%token <string> instr
%token <string> reg
%token <string> end str_byte hex_byte
%token resb resw word byte
%token baseR
%token <string> f3opcode
%token <string> f4opcode
%start prg
%type <string> labl
%type <string> index

%%

prg: 	S stmts E
	{
		addE(first_exec_loc);			//add end record

	}
	;

E: labl end labl
{
	fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "END", $3);
}

stmts:	stmt stmts
	|stmt
	;

stmt:	f1			//format 1
	|f2			//format 2
	|f3			//format 3
	|f4			//format 4
	|RB			//reserve byte
	|RW			//reserve word
	|BY			//define byte
	|WD			//define word
	|BS			//define base reg
	;

labl:	label
	{
		if(!isFoundSym($1))
		{						//symbol not found
			addToSymtab($1);			//add symbol to symtab
		}
		else if(!isDefined($1))				//symbol undefined
		{
			if(isfirst_define_text == 1)
			{					//first define text record in sequence
				modify_text_len();		//update length of previous text record
				isfirst_define_text = 0;
			}
			defineSymNow($1);			//define symbol with LOCCTR value and add text records
			cur_bytes_written = 0;
		}
		else
		{
			printf("\nDuplicate Symbol %s", $1);	//sybol predefined
			exit(1);
		}
		$$ = $1;
	}

	|
	{
		$$="";	//fix for spurious labels printed in obj.txt
	}
	;

index:	',' reg
	{
		indx = 8;		//bit x = 1
		sprintf($$, ", %s", $2);

	}
	|
	{
		indx = 0;		//bit x = 0
		$$ = malloc(100 * sizeof(char));
		sprintf($$, " ");

	}
	;

S: 	label start num
	{
		addH($1, $3, "000000");
		fprintf(objfile, "%06x \t%s\t%s\t%s\t", LOCCTR, $1, "START", $3);		//objfile
		LOCCTR = hexVal($3);
		start_addr = LOCCTR;
		printf("\nh added");
		first_exec_loc = $3;
	}

	|start num

	{
		fprintf(objfile, LOCFMT" \t\t%s\t%s\t", LOCCTR, "START", $2);
		addH("", $2, "000000");		//add header record
		LOCCTR = atoi($2);
		start_addr = LOCCTR;
		first_exec_loc = $2;
	}

	;

BS:	labl baseR num
	{
		base_reg = decVal($3);
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "BASE", $3);
	}

	| labl baseR label
	{
		base_reg = getSymLoc($3);
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "BASE", $3);
	}
	;

RB:	labl resb num
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "RESB", $3);
		int val;
		val = decVal($3);		//no. of bytes reserved
		LOCCTR += val;
	}
	;

RW:	labl resw num
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "RESW", $3);
		int val;
		val = decVal($3);		//no. of words reserved
		LOCCTR += (3*val);
	}
	;

BY:	labl byte str_byte
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "BYTE", $3);
		int i, j;
		char s[100];
		for(i=2, j=0; i<strlen($3)-1; i++, j++)
		{						//extract string part from C'<string>'
			s[j] = $3[i];				//string extracted to s
		}
		s[j] = '\0';
		LOCCTR += addTByteStr(LOCCTR, s);		//add text record
	}
	|labl byte hex_byte
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "BYTE", $3);
		int i, j, l;
		char s[100];
		for(i=2, j=0; i<strlen($3)-1; i++, j++)
		{						//extraxt hex from X'<hex>'
			s[j] = $3[i];
		}
		s[j] = '\0';
		l = strlen(s);
		addTByteHex(LOCCTR, s, l/2 + (l%2));		//add text record
		LOCCTR += l/2 + (l%2);				//(l/2)+(l%2) min bytes required for representation
	}
	|labl byte num
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "BYTE", $3);
		int n = decVal($3);				//convert num to decimal
		char s[100];
		int l = printf("%x", n);			//l is no. of characters in hex representaion
		addTByteNum(LOCCTR, n, (l/2)+(l%2));
		LOCCTR += (l/2)+(l%2);				//(l/2)+(l%2) min bytes required for representation
	}
	;

WD:	labl word str_byte
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "WORD", $3);
		long i, j;
		char s[100];
		for(i=2, j=0; i<strlen($3)-1; i++, j++)
		{
			s[j] = $3[i];
		}
		s[j] = '\0';
		LOCCTR += addTWordStr(LOCCTR, s);
	}
	|labl word hex_byte
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "WORD", $3);
		int i, j;
		char s[100];
		for(i=2, j=0; i<strlen($3)-1; i++, j++)
		{
			s[j] = $3[i];
		}
		s[j] = '\0';
		LOCCTR += addTWordHex(LOCCTR, s);
	}
	|labl word num
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, "WORD", $3);
		int n = decVal($3);
		char s[100];
		int l = printf("%x", n);
		addTByteNum(LOCCTR, n, 3*((l/2)+(l%2)));
		LOCCTR += 3*((l/2)+(l%2));
	}
	;


f1:	labl opcode
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t", LOCCTR, $1, $2);
		addT1(LOCCTR, findOpcode($2));
		LOCCTR += 1;
	}

	;

f2:	labl instr reg ',' reg
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s, %s\t", LOCCTR, $1, $2, $3, $5);
		addT2(LOCCTR, findOpcode($2), regVal($3), regVal($5));
		LOCCTR += 2;
	}
| labl instr reg
{
	fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t", LOCCTR, $1, $2, $3);
	addT2(LOCCTR, findOpcode($2), regVal($3), 0);
	LOCCTR += 2;
}
	;

f3:	labl f3opcode '#' num
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t#%s\t", LOCCTR, $1, $2, $4);
		int op, val;
		op = findOpcode($2);
		op += 1;				//set i = 1
		op = op<<4;
		val = decVal($4);
		addT3(LOCCTR, op, val);
		LOCCTR += 3;
	}

	|labl f3opcode '#' label
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t#%s\t", LOCCTR, $1, $2, $4);
		int op;
		op = findOpcode($2);
		op += 1;				//set i = 1
		calcDispAddText($4, op, 0);
		LOCCTR += 3;

	}

	|labl f3opcode '@'label index
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t@%s\t%s\t", LOCCTR, $1, $2, $4, $5);
		int op;
		op = findOpcode($2);
		op += 2;				//set n = 1
		calcDispAddText($4, op, indx);
		LOCCTR += 3;
	}

	|labl f3opcode label index
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t%s\t", LOCCTR, $1, $2, $3, $4);
		int op;
		op = findOpcode($2);
		op += 3;
		calcDispAddText($3, op, indx);
		LOCCTR += 3;
	}
	;

f4:	labl f4opcode '#' num
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t#%s\t", LOCCTR, $1, $2, $4);
		int op, val;
		op = findOpcode($2);
		op += 1;
		op = (op<<4) + 1;
		val = decVal($4);
		addT4(LOCCTR, op, val);
		LOCCTR += 4;
	}

	|labl f4opcode '#' label
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t#%s\t", LOCCTR, $1, $2, $4);
		int op;
		op = findOpcode($2);
		op += 1;
		op = (op<<4) + 1;
		addF4Text($4, op, 0);
		LOCCTR += 4;

	}

	|labl f4opcode '@' label index
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t@%s\t%s\t", LOCCTR, $1, $2, $4, $5);
		int op;
		op = findOpcode($2);
		op += 2;
		addF4Text($4, op, indx);
		LOCCTR += 4;
	}

	|labl f4opcode label index
	{
		fprintf(objfile, LOCFMT" \t%s\t%s\t%s\t%s\t", LOCCTR, $1, $2, $3, $4);
		int op,i;
		char str[5];
		for(i = 1;i < strlen($2);i++)
		str[i-1] = $2[i];
		str[i-1]='\0';
		op = findOpcode(str);
		op += 3;
		addF4Text($3, op, indx);
		LOCCTR += 4;
	}
	;
%%


void yyerror()
{
	printf("\nError\n");
	exit(1);
}

int main(int argc, char* argv[])
{
	if(argc<2)
		exit(1);
	addOp();
	createSymtab();
	output = fopen("o.txt", "w");
	yyin = fopen(argv[1], "r");
	objfile = fopen("obj.txt", "w");
	yyparse();
	if(NotDefinedError())
	{
	    printf("\n ERROR:REFERENCE TO UNDEFINED SYMBOL\n!");
        exit(0);
    }
	dispSymtab();
	//dispUndefs();
	fclose(output);
	printf("\nDone\n");
	return 0;
}
