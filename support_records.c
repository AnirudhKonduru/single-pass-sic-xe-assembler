#include<stdio.h>
#include<stdlib.h>
#include<string.h>

extern FILE *output;
extern FILE * objfile;
FILE *modptr;
int cur_bytes_written = 0;			//couont of no. of bytes in current text record
int cur_len = 0;				//count of total bytes in prog
char str[20];
int prev_mod = 0, isfirst_define_text = 1;
extern int start_addr;

void modify_text_len()
{
	char c;
	fclose(output);
	modptr = fopen("o.txt", "r+");
	fseek(modptr, prev_mod, SEEK_SET);
	while(!feof(modptr))
	{
		c = fgetc(modptr);
		if(c == 'T')
		{
			break;
		}
	}
	fseek(modptr, 8, SEEK_CUR);
	fprintf(modptr, "%02x", cur_bytes_written);
	prev_mod = ftell(modptr);
	fclose(modptr);
	output = fopen("o.txt", "a+");
}


void addH(char pname[], char sloc[], char eloc[])
{
	//print prog name, start loc and prog length
	fprintf(output, "H^%-6s^%06s^%06s", pname, sloc, eloc);
}

void addE(char loc[])
{
	modify_text_len();
	fprintf(output, "\nE^%06s", loc);				//print first executable loc
	fclose(output);
	modptr = fopen("o.txt", "r+");
	fseek(modptr, 16, SEEK_SET);
	fprintf(modptr, "%06x", LOCCTR-start_addr);
}


void addT1(int locctr, int opcode)
{
	//write a format 1 text record
	isfirst_define_text = 1;
	if(cur_bytes_written == 0)
	{								//add new text record
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
	}
	else if((cur_bytes_written + 1) > 30)
	{
		modify_text_len();
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
		cur_bytes_written = 0;
	}
	fprintf(output, "^%x", opcode);
	fprintf(objfile, "%x", opcode);
	cur_len += 1;								//one byte written
	cur_bytes_written += 1;
}

void addT2(int locctr, int opcode, int r1, int r2)
{
	//write a format 2 text record
	isfirst_define_text = 1;
	if(cur_bytes_written == 0)
	{									//add new text record
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
	}
	else if((cur_bytes_written + 1) > 30)
	{
		modify_text_len();
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
		cur_bytes_written = 0;
	}
	fprintf(output, "^%x%d%d", opcode, r1, r2);
	fprintf(objfile, "%x%d%d", opcode, r1, r2);
	cur_len += 2;								//two bytes written
	cur_bytes_written += 2;
}

void addT3(int locctr, int opcode, int val)
{
	//write a format 3 text record
	isfirst_define_text = 1;
	if(cur_bytes_written == 0)
	{									//add new text record
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
	}
	else if((cur_bytes_written + 3) > 30)
	{
		modify_text_len();
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
		cur_bytes_written = 0;
	}
	fprintf(output, "^%03x%03x", opcode, val);
	fprintf(objfile, "%03x%03x", opcode, val);
	cur_len += 3;								//three bytes written
	cur_bytes_written += 3;
}

void addT4(int locctr, int opcode, int val)
{
	//write a format 4 text record
	isfirst_define_text = 1;
	if(cur_bytes_written == 0)
	{									//add new text record
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
	}
	else if((cur_bytes_written + 4) > 30)
	{
		modify_text_len();
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
		cur_bytes_written = 0;
	}
	fprintf(output, "^%03x%05x", opcode, val);
	fprintf(objfile, "%03x%05x", opcode, val);
	cur_len += 4;								//four bytes written
	cur_bytes_written += 4;

}

void addTByteHex(int locctr, char val[], int bytes)
{
	//write a text record for byte definition of the form X'<hex>'
	isfirst_define_text = 1;
	if(cur_bytes_written == 0)
	{
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
	}
	else if((cur_bytes_written + bytes) > 30)
	{
		modify_text_len();
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
		cur_bytes_written = 0;
	}
	fprintf(output, "^");
	if(strlen(val) < 2*bytes){
		fprintf(output, "0");
		fprintf(objfile, "0");
	}
							//append 0 if odd number of characters in hexval
	fprintf(output, "%s", val);
	fprintf(objfile, "%s", val);
	cur_len += bytes;
	cur_bytes_written += bytes;
}

void addTByteNum(int locctr, int val, int bytes)
{
	//write a text record for byte definition of decimal values
	isfirst_define_text = 1;
	if(cur_bytes_written == 0)
	{
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
	}
	else if((cur_bytes_written + bytes) > 30)
	{
		modify_text_len();
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
		cur_bytes_written = 0;
	}
	fprintf(output, "^%.*x", bytes*2, val);
	fprintf(objfile, "%.*x", bytes*2, val);
	cur_len += bytes;
	cur_bytes_written += bytes;
}


int addTWordHex(int locctr, char val[])
{
	//write a text record for word definition of the form X'<hex>'
	isfirst_define_text = 1;
	int i, bytes, n = 0;

	if(strlen(val)%6 != 0)
		n = ((strlen(val)/6)+1)*6 - strlen(val);

	bytes = (strlen(val) + n)/2;

	if(cur_bytes_written == 0)
	{
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
	}
	else if((cur_bytes_written + bytes) > 30)
	{
		modify_text_len();
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
		cur_bytes_written = 0;
	}
	fprintf(output, "^");
	for(i=0; i<n; i++){
		fprintf(output, "0");
		fprintf(objfile, "0");
	}
	fprintf(output, "%s", val);
	fprintf(objfile, "%s", val);
	cur_len += bytes;
	cur_bytes_written += bytes;
	return bytes;
}

int addTByteStr(int locctr, char s[])
{
	//write a text record for byte definition of the form C'<hex>'
	isfirst_define_text = 1;
	int bytes = strlen(s), i;
	if(cur_bytes_written == 0)
	{
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
	}
	else if((cur_bytes_written + bytes) > 30)
	{
		modify_text_len();
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
		cur_bytes_written = 0;
	}
	fprintf(output, "^");
	for(i=0; i<strlen(s); i++){
		fprintf(output, "%02x", (int)s[i]);
		fprintf(objfile, "%02x", (int)s[i]);
	}
	cur_len += bytes;
	cur_bytes_written += bytes;
	return bytes;
}

int addTWordStr(int locctr, char s[])
{
	//write a text record for word definition of the form C'<hex>'
	isfirst_define_text = 1;
	int bytes = strlen(s), i;
	if(cur_bytes_written == 0)
	{
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
	}
	else if((cur_bytes_written + bytes) > 30)
	{
		modify_text_len();
		fprintf(output, "\n");
		fprintf(output, "T^%06x^%02x", locctr, 0);
		cur_bytes_written = 0;
	}
	fprintf(output, "^");
	for(i=0; i<strlen(s); i++){
			fprintf(output, "%06x", (int)s[i]);
			fprintf(objfile, "%06x", (int)s[i]);
	}
	cur_len += 3*bytes;
	cur_bytes_written += 3*bytes;
	return 3*bytes;
}
