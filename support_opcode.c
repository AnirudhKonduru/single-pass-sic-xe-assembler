#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct op
{
	char *name;
	int val;
	struct op * next;
};

typedef struct op* optab;

optab ophead;

void createOptab()
{
	ophead = (optab)malloc(sizeof(optab));
	ophead->val = 0;
	ophead->name = "myop";
	ophead->next = NULL;
	printf("\nOptab Created");

}

void dispOptab()
{
	optab temp;
	temp = ophead;
	while(temp != NULL)
	{
		printf("\n%s----%d", temp->name, temp->val);
		temp = temp->next;
	}
}


void addToOptab(char s[], int v)
{
	optab temp, node;
	node = (optab) malloc(sizeof(optab));
	node->name = s;
	node->next = NULL;
	node->val = v;

	temp = ophead;
	while(temp->next != NULL)
	{
		temp = temp->next;
	}
	temp->next = node;
}


int findOpcode(char s[])
{
	optab temp;
	temp = ophead;
	while(temp != NULL)
	{
		if(strcmp(temp->name, s) == 0)
		{
			return temp->val;
		}
		temp = temp->next;
	}
	if(temp == NULL)
		return NULL;
}


void addOp()
{
	createOptab();
	addToOptab("ADD", 24);
	addToOptab("ADDF", 88);
	addToOptab("ADDR", 144);
	addToOptab("AND", 64);
	addToOptab("CLEAR", 180);
	addToOptab("COMP", 40);
	addToOptab("COMPF", 136);
	addToOptab("COMPR", 160);
	addToOptab("DIV", 36);
	addToOptab("DIVF", 100);
	addToOptab("DIVR", 156);
	addToOptab("FIX", 196);
	addToOptab("FLOAT", 192);
	addToOptab("HIO", 244);
	addToOptab("J", 60);
	addToOptab("JEQ", 48);
	addToOptab("JGT", 52);
	addToOptab("JLT", 56);
	addToOptab("JSUB", 72);
	addToOptab("LDA", 00);
	addToOptab("LDB", 104);
	addToOptab("LDCH", 80);
	addToOptab("LDF", 112);
	addToOptab("LDL", 8);
	addToOptab("LDS", 108);
	addToOptab("LDT", 116);
	addToOptab("LDX", 4);
	addToOptab("LPS", 208);
	addToOptab("MUL", 32);
	addToOptab("MULF", 96);
	addToOptab("MULR", 152);
	addToOptab("NORM", 200);
	addToOptab("OR", 68);
	addToOptab("RD", 216);
	addToOptab("RMO", 172);
	addToOptab("RSUB", 76);
	addToOptab("SHIFTL", 164);
	addToOptab("SHIFTR", 168);
	addToOptab("SIO", 240);
	addToOptab("SSK", 236);
	addToOptab("STA", 12);
	addToOptab("STB", 120);
	addToOptab("STCH", 84);
	addToOptab("STF", 128);
	addToOptab("STI", 212);
	addToOptab("STL", 20);
	addToOptab("STS", 124);
	addToOptab("STSW", 232);
	addToOptab("STT", 132);
	addToOptab("STX", 16);
	addToOptab("SUB", 28);
	addToOptab("SUBF", 92);
	addToOptab("SUBR", 148);
	addToOptab("SVC", 176);
	addToOptab("TD", 224);
	addToOptab("TIO", 248);
	addToOptab("TIX", 44);
	addToOptab("TIXR", 184);
	addToOptab("WD", 220);
	printf("\nAdded all opcodes");
}
