%{

#include"y.tab.h"

%}

Format1 ("FIX"|"FLOAT"|"HIO"|"NORM"|"SIO"|"TIO")
Format2 ("ADDR"|"CLEAR"|"COMPR"|"DIVR"|"MULR"|"RMO"|"SHIFTL"|"SHIFTR"|"SUBR"|"SVC"|"TIXR")
Format3 ("ADD"|"ADDF"|"AND"|"COMP"|"COMPF"|"DIV"|"DIVF"|"J"|"JEQ"|"JGT"|"JLT"|"JSUB"|"LDA"|"LDB"|"LDCH"|"LDF"|"LDL"|"LDS"|"LDT"|"LDX"|"LPS"|"MUL"|"MULF"|"OR"|"RD"|"RSUB"|"SSK"|"STA"|"STB"|"STCH"|"STF"|"STI"|"STL"|"STS"|"STSW"|"STT"|"STX"|"SUB"|"SUBF"|"TD"|"TIX"|"WD")
Format4 ("+ADD"|"+ADDF"|"+AND"|"+COMP"|"+COMPF"|"+DIV"|"+DIVF"|"+J"|"+JEQ"|"+JGT"|"+JLT"|"+JSUB"|"+LDA"|"+LDB"|"+LDCH"|"+LDF"|"+LDL"|"+LDS"|"+LDT"|"+LDX"|"+LPS"|"+MUL"|"+MULF"|"+OR"|"+RD"|"+RSUB"|"+SSK"|"+STA"|"+STB"|"+STCH"|"+STF"|"+STI"|"+STL"|"+STS"|"+STSW"|"+STT"|"+STX"|"+SUB"|"+SUBF"|"+TD"|"+TIX"|"+WD")
Directive ("START"|"END"|"EQU"|"ORG"|"BASE"|"LTORG"|"RESW"|"RESB"|"BYTE"|"NOBASE"|"WORD")
Reg ("A"|"X"|"L"|"PC"|"SW"|"B"|"S"|"T"|"F")

%%

"RESB" 			{return resb;}
"RESW" 			{return resw;}
"WORD" 			{return word;}
"BYTE" 			{return byte;}
"BASE"			{return baseR;}
"START" 		{return start;}
"END" 			{return end;}


"C'"[^"'"]*.*"'" 	{yylval.string = strdup(yytext); return str_byte;}
"X'"[^"'"]*[A-Z0-9]*"'" {yylval.string = strdup(yytext); return hex_byte;}
{Reg} 			{yylval.string = strdup(yytext); return reg;}
{Format1} 		{yylval.string = strdup(yytext); return opcode;}
{Format2} 		{yylval.string = strdup(yytext); return instr;}
{Format3} 		{yylval.string = strdup(yytext); return f3opcode;}
{Format4} 		{yylval.string = strdup(yytext); return f4opcode;}



[a-zA-Z]+ 	{
			yylval.string = strdup(yytext);	
			return label;
		}

[0-9ABCDEF]+ 	{
			yylval.string = strdup(yytext);	
			return num;
		}

[,#@+] return yytext[0];
[ \n] ;

%%

int yywrap() 
{
	return 1;
}


