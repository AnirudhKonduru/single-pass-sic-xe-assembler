#!/usr/bin/env bash

# run the assembler with the input file
./assembler $1
echo -e "Objcode generated in obj.txt\n"

echo -e "  ====="$1"=====  \t\t\t" "\t\t\t  =====obj.txt=====  \t" "\n"
# print the input file and obj file side-by-side
pr -m -T -w100 $1 obj.txt
echo -e "\n\n"

# print the assembly listing
cat o.txt
echo -e "\n\n"
