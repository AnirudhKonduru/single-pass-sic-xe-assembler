assembler: lex.yy.c y.tab.c
	gcc lex.yy.c y.tab.c -o assembler
lex.yy.c: tokens.l y.tab.h
	lex tokens.l
y.tab.c: parser.y
	yacc -d parser.y
y.tab.h: parser.y
	yacc -d parser.y
clean:
	rm -rf lex.yy.c y.tab.c y.tab.h a.out assembler obj.txt o.txt

.SILENT: clean

all: assembler
