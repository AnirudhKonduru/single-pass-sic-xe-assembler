# Single Pass Assembler for SIC/XE

Built using lex and yacc.

#### Setting up the system

  You'll need a linux distribution to run this, so either
  
  1. Install a linux distribution like Ubuntu,
  2. Install the Linux subsystem for Windows 10, from the Windows Store.
  3. Set up a virtual machine.

#### Installing, Compiling, etc

Open up the terminal and run:
  `sudo apt-get install flex bison git make`

This will install the necessary tools.

Then run:
  `git clone https://bitbucket.org/AnirudhKonduru/single-pass-sic-xe-assembler`
  to download the code.

  Go into the directory with: `cd ./single-pass-sic-xe-assembler`

  Then run `make clean` to remove any unnecessary files, followed by a `make` to compile the assembler.

  Now running `./assembler input.txt` will run the program with input from input.txt. The assembly listing is put in o.txt and the objcode in obj.txt.

  If you'd like to print everything out to the terminal after running, you can run `./run.sh input.txt`. This will run `./assembler input.txt` for you, and also prints out the input file with the obj code, side by side, followed by the assembly listing.
